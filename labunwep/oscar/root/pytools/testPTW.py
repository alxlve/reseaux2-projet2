from PTW import guess
from Crypto.Cipher import ARC4
from random import randint

def randstr(len):
    s = ''
    for i in range(len):
        s = s + chr(randint(0, 255))
    return s

def stream(key, iv, len):
    """returns the len first bytes of RC4(iv+key)"""
    c = ARC4.new(iv+key)
    return c.encrypt('\0' * len)

def test(key,nbp):
    """generates nbp pairs (iv,X) using the secret
       key key then apply PTW.guess on the list"""
    n = len(key)
    ivXs = []
    for i in range(nbp):
        iv = randstr(3)
        #iv = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(3)) # bad idea...
        x = stream(key, iv, n+2)
        ivXs.append((iv, x))
        print 'iv: %s | x: %s' % (iv, x)
    return guess(ivXs, n)

if __name__ == "__main__":
    from sys import argv
    if len(argv) >= 2:
        key = argv[1]
    else:
        key = 'lapin'
    print 'Generating 60000 (iv, x) pairs for key %s.' % (key,)
    print 'Guessing %s.' % (repr(test(key,60000)),)
