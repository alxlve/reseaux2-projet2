HEADER = '\033[95m'
BLUE = '\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
ENDC = '\033[0m'

def infog(msg):
    print GREEN + msg + ENDC

def info(msg):
    print BLUE + msg + ENDC

def warn(msg):
    print YELLOW + msg + ENDC

def err(msg):
    print RED + msg + ENDC
