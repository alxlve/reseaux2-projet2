import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR) # For no IPv6 warning...
from scapy.all import *
from struct import unpack

arp = int("0x0806", 16)

def amac(s):
  return ":".join(map(lambda x: "%02x" % x, unpack("6B", s)))

def analyze(pkt):
    cipherLoad = pkt[Raw].load
    if len(cipherLoad) == 49: # 42 + 7 = ARP ciphered Packet size
        (dst, src, proto, iv) = unpack("!6s6sH3s", cipherLoad[:17])
        src = amac(src)
        dst = amac(dst)
        siv="%02x%02x%02x" % tuple(map(ord, iv))
        if proto == arp: # This is sure ARP!
            #print pkt.command()
            #print pkt.show()
            if dst == "ff:ff:ff:ff:ff:ff": # This is sure a 'Who has X ? in broadcast'.
                print "%s > %s (ARP).\n %4d bytes (IV = %s), broadcast request." % (src, dst, len(cipherLoad), siv)
            else:
                print "%s > %s (ARP).\n %4d bytes (IV = %s), unicast request or reply." % (src, dst, len(cipherLoad), siv)

print "Sniffing ARP ciphered packets...\n"
sniff(iface = "eth0", prn = analyze, store = 0, \
      lfilter = lambda pkt: pkt.haslayer(UDP) and pkt[UDP].dport == 10053 and pkt[UDP].sport == 10053)
