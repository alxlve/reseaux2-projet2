from socket import *
from struct import unpack

def amac(s):
  return ":".join(map(lambda x: "%02x" % x, unpack("6B",s)))

s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
s.setsockopt(SOL_SOCKET,SO_BROADCAST,1)
s.bind(('0.0.0.0',10053))
while True:
    x=s.recv(4096)
    (dst,src,proto,iv)=unpack("!6s6sH3s",x[:17])
    encrypted=x[17:]
    src=amac(src)
    dst=amac(dst)
    siv="%02x%02x%02x" % tuple(map(ord,iv))
    print "%s > %s (%x). %4d bytes (IV=%s)" % (src,dst,proto,len(x),siv)
