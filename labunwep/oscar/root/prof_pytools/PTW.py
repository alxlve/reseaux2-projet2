"""
A naive implementation of the PTW key retrival algorithm
for WEP-like RC4 encoding. Given a list of pairs (iv,X)
where each X is a prefix of size n+2 of the keystream
RC4(iv+key), the function guess retrieves the secret key
key of length n with high probability -- appart from
so-called "strong keys" not considered here.
-- N. Ollinger 2015
"""

def threeKSA(iv):
    """Compute j and S after 3 rounds of KSA"""
    S=range(256)
    j=0
    for i in range(3):
        j=(j+S[i]+ord(iv[i])) % 256
        S[i],S[j] = S[j],S[i]
    return (S,j)

def Ai((S,j),X,i):
    """Compute PTW parameter Ai given (S,j) from threeKSA,
       encrypted prefix X and index i"""
    v=S.index((3+i-ord(X[2+i])) % 256)-j
    for m in range(i+1):
        v=v-S[m+3]
    return v % 256

def step(iv,X):
    """Compute the first n Ai given IV iv and an encrypted 
       prefix X of length n+2"""
    z=threeKSA(iv)
    return [Ai(z,X,i) for i in range(len(X)-2)]

def guess(ivXs,n):
    """PTW guess of the key given a list of pairs (iv,X)
       and the size n of the key (the prefixes should be
       of length n+2)"""
    stats=[ {} for i in range(n) ]
    for (iv,x) in ivXs:
        y=step(iv,x)
        for i in range(n):
            t=stats[i]
            v=y[i]
            if t.has_key(v):
               t[v]=t[v]+1
            else:
               t[v]=1
    sigma=[]
    for i in range(n):
        t=stats[i]
        occ=0
        v=0
        for k in t.iterkeys():
            if t[k]>occ:
                v=k
                occ=t[k]
        sigma.append(v)
    theta=chr(sigma[0])
    for i in range(1,n):
        theta=theta+(chr((sigma[i]-sigma[i-1])%256))
    return theta

