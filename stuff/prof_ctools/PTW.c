#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "PTW.h"

static u8 S[256];

static u8 threeKSA(u8 *iv)
{
    register int k,i,j;

    for(k=0;k<256;k++) S[k]=k;
    i=j=0;
    for(i=0;i<3;i++) {
        register int v;
        j=(iv[i]+S[i]+j)%256;
        v=S[i]; S[i]=S[j]; S[j]=v;
    }
    return j;
}

static u8 Ai(u8 j, u8 *X, int i)
{
    register int k,m;
    u8 v;

    v=(3+i-X[2+i]) % 256;
    for(k=0;S[k]!=v;k++);
    v=k-j;
    for(m=0;m<=i;m++) v-=S[m+3];
    return v;
}

static void step(u8 *iv, u8 *X, int n, u8 *result)
{
    u8 j;
    int i;

    j=threeKSA(iv);
    for(i=0;i<n;i++)
        result[i]=Ai(j,X,i);
}

void guess(struct ivX ivXs[], int nbp, int n, u8 *myguess)
{
    int i,j,maxo[n],occ,v, *t;
    u8 r[n];

    t=(int *)malloc(n*256*sizeof(int));
    memset(t,0,n*256);
    memset(maxo,0,n);
    for(j=0;j<nbp;j++) {
        step(ivXs[j].iv,ivXs[j].x,n,r);
        for(i=0;i<n;i++) t[256*i+r[i]]++;
    }
    for(i=0;i<n;i++) {
        v=occ=0;
        for(j=0;j<256;j++)
            if (t[256*i+j]>occ) {
                occ=t[256*i+j];
                v=j;
            }
        maxo[i]=v;
    }
    myguess[0]=maxo[0];
    for(i=1;i<n;i++) myguess[i]=maxo[i]-maxo[i-1];
}
