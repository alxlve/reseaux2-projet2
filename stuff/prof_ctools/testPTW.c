#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "PTW.h"

static u8 S[256];

void rc4(const u8 *key, int klen, u8 *res, int len)
{
    register int k,i,j;

    for(k=0;k<256;k++) S[k]=k;
    i=j=0;
    for(k=0;k<256;k++) {
        register int v;
        j=(key[i]+S[k]+j)%256;
        v=S[k]; S[k]=S[j]; S[j]=v;
        i=(i+1)%klen;
    }
    i=j=0;
    for(k=0;k<len;k++) {
        i=(i+1)%256;
        j=(j+S[i])%256;
        { register int v; v=S[i]; S[i]=S[j]; S[j]=v; }
        res[k] = S[(S[i]+S[j])%256];
    }
}

void randstr(u8 *s, int n)
{
  int i;

  for(i=0;i<n;i++)
     s[i]=random()%256; 
}

void out(const char *s, int n)
{
    int i;
    unsigned char c;

    for(i=0;i<n;i++) {
        c=s[i];
        if (isprint(c))
            printf("%c",c);
        else
            printf("\\x%02x",c);
    }
}

void test(u8 *key, int klen, int nbp, u8 *res)
{
    struct ivX ivXs[nbp+1];
    u8 ivs[3*nbp], xs[(klen+2)*nbp], *iv, *x;
    u8 ivkey[klen+3];
    int i,m;

    iv=ivs;
    x=xs;
    m=klen+2;
    memcpy(ivkey+3,key,klen);
    for(i=0;i<nbp;i++) {
      randstr(iv,3);
      memcpy(ivkey,iv,3);
      rc4(ivkey,klen+3,x,m);
      ivXs[i].iv=iv;
      ivXs[i].x=x;
      iv+=3;
      x+=m;
    }
    guess(ivXs,nbp,klen,res);
}

#define NBTEST 60000

int main(int argc, char *argv[])
{
    int klen;
    u8 *key,res[1024];

    srandom(time(NULL));
    if (argc>1)
        key=(u8 *)(argv[1]);
    else
        key=(u8 *)"lapin";
    klen=strlen((const char *)key);
    printf("Generation %d (iv,x) pairs for key %s.\n",NBTEST,key);
    test(key,klen,NBTEST,res);
    printf("Guessing ");
    out((const char *)res,klen);
    printf(".\n");
}
