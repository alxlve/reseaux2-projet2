#ifndef PTW_H
#define PTW_H
typedef unsigned char u8;

struct ivX {
    u8 *iv;
    u8 *x;
};

void guess(struct ivX ivXs[], int nbp, int n, u8 *myguess);
#endif /* PTW_H */
