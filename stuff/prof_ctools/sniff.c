#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include <arpa/inet.h>

int main(int argc, char *argv[])
{
    int s;
    struct sockaddr_in addr;
    int o;
    unsigned char buf[4096];
    int len;

    if ((s=socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1) {
      perror("socket");
      exit(1);
    }
    o=1;
    if (setsockopt(s,SOL_SOCKET,SO_BROADCAST,(void *) &o,sizeof(o))==-1) {
       perror("setsockopt");
       exit(1);
   }
   memset(&addr, 0, sizeof(addr));   
   addr.sin_family = AF_INET;                 
   addr.sin_addr.s_addr = INADDR_ANY;
   addr.sin_port = htons(10053);   
   if (bind(s,(struct sockaddr *)&addr,sizeof(addr))==-1) {
       perror("bind");
       exit(1);
   }
   while(1) {
     if ((len=recv(s,buf,4096,0))==-1){
            perror("recvfrom");
            exit(1);
     }
     printf("%02x:%02x:%02x:%02x:%02x:%02x > %02x:%02x:%02x:%02x:%02x:%02x (%x%02x). %4d bytes (IV=%02x%02x%02x)\n",buf[6],buf[7],buf[8],buf[9],buf[10],buf[11],buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[12],buf[13],len,buf[14],buf[15],buf[16]);
   }
}
